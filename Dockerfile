# Imports
FROM registry.gitlab.com/txlab/tools/bson-split-docs as bson-split-docs

# Main image
FROM alpine:3.15
LABEL maintainer="Manu [tennox]"

RUN apk add --no-cache git openssh-client rsync rclone
RUN adduser -Du 1000 user

COPY --from=bson-split-docs /bin/bson-split-docs /bin/bson-split-docs

USER 1000

# RUN wget https://gitlab.com/txlab/tools/bson-split-docs/-/jobs/2667110299/artifacts/download \
#   && unzip download -d /usr/local/bin \
#   && ls -al /usr/local/bin \
#   && chmod +x /usr/local/bin/bson-split-docs
# RUN wget https://gitlab.com/txlab/tools/bson-split-docs/-/jobs/2670425824/artifacts/raw/bson-split-docs \
#   -O /usr/local/bin/bson-split-docs \
#   && chmod +x /usr/local/bin/bson-split-docs